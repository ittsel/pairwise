require "pairwise/version"
require 'active_resource'

module Pairwise
  autoload :Resource,   'pairwise/resource'
  autoload :Question,   'pairwise/question'
  autoload :Prompt,     'pairwise/prompt'
  autoload :Choice,     'pairwise/choice' 
  autoload :Vote,       'pairwise/vote' 
  autoload :Skip,       'pairwise/skip'


  
  # to configure resource
  mattr_reader :resource
  @@resource = ActiveResource::Base


  def self.configuration
    yield self
  end

  class Error < StandardError; end
end
