module Pairwise
  class Vote < Resource
    self.element_name = "vote"
    self.prefix = '/questions/:question_id/prompts/:prompt_id/'
    
    def initialize(attributes = {}, persisted = false)
      @attributes     = {}.with_indifferent_access
      @prefix_options = {}
      @persisted = persisted
      load(attributes, remove_root=false)
    end

    def self.collection_name
      element_name
    end

    def to_xml(options={})
      super(options)
    end

  end
end

# Pairwise::Vote.create(question_id: 1, prompt_id: 1, data: "asd")
#  Pairwise.configuration

