module Pairwise
  class Question < Resource
    attr_accessor :ques_cus_lovs_attributes
    # def initialize(attributes = {}, persisted = false)
    #   @attributes     = {}.with_indifferent_access
    #   @prefix_options = {}
    #   @persisted = persisted
    #   load(attributes, remove_root=false)
    # end

    def body
      self.name
    end

    def self.where(clauses = {})
      super(clauses.merge(default__params))
    end

    def self.find(*arguments)
      scope            = arguments.slice!(0)
      options          = arguments.slice!(0) || {}
      options[:params] ? options[:params].merge!(default__params) : options[:params] = default__params 

      case scope
        when :all   then find_every(options)
        when :first then find_every(options).first
        when :last  then find_every(options).last
        when :one   then find_one(options)
        else             find_single(scope, options)
      end
    end

    def master_category
      ::MasterCategory.find(self.master_cat_id)
    end

    def master_sub_category
      ::MasterSubCategory.find(self.master_sub_cat_id)
    end

    def master_question
      ::MasterQuestion.find(self.master_ques_id)
    end

    def ques_cus_lovs
      ::QuesCusLov.where(pairwise_question_id: self.id)
    end

    def category
      ::Category.find(self.category_id)
    end

    def create_batch(object, parent)
      return if object.nil?
      object.attributes.each do |k,r|
        attrs = r.attributes.merge(parent).except(:_destroy)
        ::QuesCusLov.create(attrs)
      end
    end
    
    def locked
      true
    end

    def locked=(bool)
      true
    end

    def deep_clone
      self.clone
    end

    def create
      delete_attributes
      resp = super
      parent = {pairwise_question_id: Hash.from_xml(resp.body)['question']['id']}
      create_batch(@ques_cus_lovs_attributes, parent) 
      resp
    end

    private

    

    def delete_attributes
      ques_cus_lovs_attributes = @attributes[:ques_cus_lovs_attributes] if @ques_cus_lovs_attributes.nil?
      @attributes = @attributes.except(:ques_cus_lovs_attributes, :_destroy)
    end

    def self.default__params
      {
        with_appearance:    true,
        with_prompt:        true,
        with_visitor_stats: true
      }
    end

  end
end

